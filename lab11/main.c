#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gaussj.c"
#include "../numerical_library/sinft.c"
#include "../numerical_library/realft.c"
#include "../numerical_library/four1.c"


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define RANDOM ((rand()/(RAND_MAX + 1.0f)))
#define SIGNUM (((RANDOM) > (1/2.0)) ? 1 : -1)

#define MAX(x, y) (((x)>(y)) ? (x) : (y))
#define ABS(x) (((x) < 0) ? (-(x)) : (x))

#define PI (4*atan(1))

float foo(int i, int n){
	float omega = 4*PI/(n*1.0f);
	return sin(omega*i) + sin(2*omega*i) + sin(3*omega*i);
}

int main(){
	srand(time(NULL));
	const char *filename = "w_k8.dat";
	int k = 8;
	int n = 1 << k;

	float *y0 = vector(1, n);
	float *ySzum = vector(1, n);
	float *y = vector(1, n);
	float *yTrans = vector(1, n);

	for(int i=1; i<=n; i++){
		y0[i] = foo(i, n);
		ySzum[i] = y0[i] + 2*SIGNUM*RANDOM;
		y[i] = ySzum[i];
	}

	sinft(y, n);

	for(int i=1; i<=n; i++){
		yTrans[i] = y[i];
	}

	float max = y[1];
	for(int i=2; i<=n; i++){
		max = MAX(ABS(y[i]), max);
	}

	for(int i=1; i<=n; i++){
		if(ABS(y[i]) < max/4) y[i] = 0.0;
	}

	for(int i=1; i<=n; i++){
		y[i] *= 2/(1.0f*n);
	}

	sinft(y, n);

	FILE *f = fopen(filename, "w");

	for(int i=1; i<=n; i++){
		fprintf(f, "%d\t%f\t%f\t%f\t%f\n", i, y0[i], ySzum[i], y[i], yTrans[i]);
	}

	fclose(f);

	return 0;
}
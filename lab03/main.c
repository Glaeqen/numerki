#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gaussj.c"
#include "math.h"
#include <stdio.h>
#include <time.h>

#define max(X,Y) ((X)>(Y)? (X):(Y))
#define min(X,Y) ((X)<(Y)? (X):(Y))
#define abs(X) ((X)>0? (X):-(X))

#define m 5

void matrixTimesVector(double **leftMatrix, double *rightVector, double *outputVector, int size);
double vectorTimesVector(double *leftVector, double *rightVector, int size);
void vectorMinusVector(double *leftVector, double *rightVector, double *outputVector, int size);
void vectorPlusVector(double *leftVector, double *rightVector, double *outputVector, int size);
void clearVector(double *vector, int size);
void scalarTimesVector(double *vector, double scalar, int size);
void copyVector(double *sourceVector, double *targetVector, int size);

int main(void){
	time_t t1, t2;
	double t12;
	time(&t1);

	int n = 1000;
	double **A = dmatrix(1, n, 1, n);
	// int m = 5; #define

	for(int i=1; i<=n; i++){
		for(int j=1; j<=n; j++){
			if(abs(i-j)<=m) A[i][j] = 1/(1.0+abs(i-j));
			else A[i][j] = 0.0; 
		}
	}

	double *b = dvector(1, n);

	for(int i=1; i<=n; i++){
		b[i] = i;
	}

	double rr = 1.0;
	int maxIterator = 0;

	double *x = dvector(1, n);
	double *prevX = dvector(1, n);
	double *tmp1 = dvector(1, n);
	double *tmpR = dvector(1, n);
	clearVector(x, n);
	clearVector(prevX, n);

	while(rr > 0.000001 && maxIterator <= 500){
		maxIterator++;
		clearVector(tmp1, n);
		clearVector(tmpR, n);
		//r = b-(A*x)
		matrixTimesVector(A, x, tmp1, n);
		vectorMinusVector(b, tmp1, tmpR, n);
		//l = r^T * r
		double l = vectorTimesVector(tmpR, tmpR, n);
		//z = A*r
		clearVector(tmp1, n);
		matrixTimesVector(A, tmpR, tmp1, n);
		//w = r^t*z
		double w = vectorTimesVector(tmpR, tmp1, n);

		double alpha = l/w;

		//x = x+alpha*r
		clearVector(tmp1, n);
		scalarTimesVector(tmpR, alpha, n);
		vectorPlusVector(prevX, tmpR, x, n);
		copyVector(x, prevX, n);

		rr = sqrt(l);

		printf("%d \t %15.8f \t %15.8f\n", maxIterator, rr, sqrt(vectorTimesVector(x, x, n)));
	}


	time(&t2);
	t12 = difftime(t2, t1);
	return 0;
}

void matrixTimesVector(double **leftMatrix, double *rightVector, double *outputVector, int size){
	for(int i=1; i<=size; i++){
		int jmin = max(1, i-m);
		int jmax = min(size, i+m);
		outputVector[i] = 0.0;
		for(int j=jmin; j<=jmax; j++){
			outputVector[i] += leftMatrix[i][j]*rightVector[i];
		}
	}
}

double vectorTimesVector(double *leftVector, double *rightVector, int size){
	double result = 0.0;
	for(int i=1; i<=size; i++){
		result += leftVector[i] * rightVector[i];
	}
	return result;
}

void vectorMinusVector(double *leftVector, double *rightVector, double *outputVector, int size){
	for(int i=1; i<=size; i++){
		outputVector[i] = leftVector[i] - rightVector[i];
	}
}

void vectorPlusVector(double *leftVector, double *rightVector, double *outputVector, int size){
	for(int i=1; i<=size; i++){
		outputVector[i] = leftVector[i] + rightVector[i];
	}
}

void clearVector(double *vector, int size){
	for(int i=1; i<=size; i++){
		vector[i] = 0.0;
	}
}

void scalarTimesVector(double *vector, double scalar, int size){
	for(int i=1; i<=size; i++){
		vector[i] *= scalar;
	}
}

void copyVector(double *sourceVector, double *targetVector, int size){
	for(int i=1; i<=size; i++){
		targetVector[i] = sourceVector[i];
	}
}
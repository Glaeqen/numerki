#include "../numerical_library/nr.h"
#include "../numerical_library/gauleg.c"
#include "../numerical_library/gaulag.c"
#include "../numerical_library/gauher.c"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gammln.c"


#include <stdio.h>
#include <math.h>

#define ABS(x) (((x)<0) ? ((-(x))) : ((x)))

float c1(float x){
	return 1/(x*sqrt(x*x-1));
}

float c2(float x){
	return log(ABS(x))*exp(-(x*x));
}

float c3(float x){
	return sin(2*x)*exp(-2*x);
}


int main(){
	float *integralOutput = vector(1, 100);
	float *nodes = vector(1, 100);
	float *wsp = vector(1, 100);
	for(int n=2; n<=100; n++){
		for(int i=1; i<=100; i++){
			nodes[i] = wsp[i] = 0.0;
		}
		float integral = 0;
		gauleg(1.0, 2.0, nodes, wsp, n);
		for(int i=1; i<=n; i++){
			integral += wsp[i]*c1(nodes[i]);
		}
		integralOutput[n] = integral;
	}
	float preciseOut = 4*atan(1)/3.0;
	FILE *f = fopen("c1.dat", "w");
	for(int i=2; i<=100; i++){
		fprintf(f, "%d\t%f\n", i, ABS(integralOutput[i] - preciseOut));
	}
	fclose(f);

	for(int n=2; n<=100; n+=2){
		for(int i=1; i<=100; i++){
			nodes[i] = wsp[i] = 0.0;
		}
		float integral = 0;
		gauher(nodes, wsp, n);
		for(int i=1; i<=n; i++){
			integral += wsp[i]*c2(nodes[i])/2.0;
		}
		integralOutput[n] = integral;
	}
	preciseOut = -0.8700577;
	f = fopen("c2-her.dat", "w");
	for(int i=2; i<=100; i+=2){
		fprintf(f, "%d\t%f\n", i, ABS(integralOutput[i] - preciseOut));
	}
	fclose(f);

	for(int n=2; n<=100; n++){
		for(int i=1; i<=100; i++){
			nodes[i] = wsp[i] = 0.0;
		}
		float integral = 0;
		gauleg(0.0, 5.0, nodes, wsp, n);
		for(int i=1; i<=n; i++){
			integral += wsp[i]*c2(nodes[i]);
		}
		integralOutput[n] = integral;
	}
	f = fopen("c2-leg.dat", "w");
	for(int i=2; i<=100; i++){
		fprintf(f, "%d\t%f\n", i, ABS(integralOutput[i] - preciseOut));
	}
	fclose(f);


	for(int n=2; n<=10; n++){
		for(int i=1; i<=100; i++){
			nodes[i] = wsp[i] = 0.0;
		}
		float integral = 0;
		gaulag(nodes, wsp, n, 0);
		for(int i=1; i<=n; i++){
			integral += wsp[i]*c3(nodes[i]);
		}
		integralOutput[n] = integral;
	}
	f = fopen("c3.dat", "w");
	preciseOut = 2/13.0;
	for(int i=2; i<=10; i++){
		fprintf(f, "%d\t%f\n", i, ABS(integralOutput[i] - preciseOut));
	}
	fclose(f);
	return 0;
}


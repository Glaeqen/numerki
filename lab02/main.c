#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gaussj.c"
#include "math.h"
#include <stdio.h>

int main(void){
	float *d, *c, *a, *l, *u, *v, *b, *y;
	int N = 50; // 500
	float aa = 0.5; //Value of 'a' from .pdf - conflict with 'a' vector
	float bb = 2; //Value of 'b' from .pdf - conflict with 'b' vector
	float h = 2*bb/(N-1);

	d = (float *)malloc((N+1)*sizeof(float));
	c = (float *)malloc((N+1)*sizeof(float));
	a = (float *)malloc((N+1)*sizeof(float));
	l = (float *)malloc((N+1)*sizeof(float));
	u = (float *)malloc((N+1)*sizeof(float));
	v = (float *)malloc((N+1)*sizeof(float));
	b = (float *)malloc((N+1)*sizeof(float));
	y = (float *)malloc((N+1)*sizeof(float));

	b[1] = 0;
	b[N-1] = 0;
	for(int i=2; i<=N-1; i++){
		float xi = -bb+h*(i-1);
		b[i] = 0.0;
		if(xi>=(-aa) && xi<0)	b[i] = -1;
		if(xi>0 && xi<=aa)	b[i] = 1;
	}

	d[1] = 1;
	c[1] = 0;
	a[N] = 0;
	for(int i=2; i<=N; i++){
		d[i] = -2/(h*h);
		c[i] = 1/(h*h);
		a[i-1] = 1/(h*h);
	}


	u[1] = d[1];
	for(int i=2; i<=N; i++){
		l[i] = a[i] / u[i-1];
		u[i] = d[i] - l[i]*c[i-1];
	}

	y[1] = b[1];
	for(int i=2; i<=N; i++){
		y[i] = b[i] - l[i]*y[i-1];
	}

	v[N] = y[N]/u[N];

	for(int i = N-1; i>=1; i--){
		v[i] = (y[i] - c[i]*v[i+1])/u[i];
	}

	FILE *file;

	file = fopen("dane.dat", "w");
	float analityczneV = 0.0;
	for(int i=1; i<=N; i++){
		float xi = -bb+h*(i-1);
		if(xi>=(-bb) && xi<=(-aa)){
			analityczneV = xi/16+1.0/8;
		}
		if(xi>(-aa) && xi<= 0){
			analityczneV = -(xi*xi)/2-(7*xi)/16;
		}
		if(xi>0 && xi<=aa){
			analityczneV = (xi*xi)/2-(7*xi)/16;
		}
		if(xi>aa && xi<=bb){
			analityczneV = xi/16-1.0/8;
		}
		fprintf(file, "%f %f %f\n", xi, v[i], analityczneV);
	}

	fclose(file);

	free(d);
	free(c);
	free(a);
	free(l);
	free(u);
	free(v);
	free(b);
	free(y);


	return 0;
}
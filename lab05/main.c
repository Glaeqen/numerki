#include "../numerical_library/nrutil.c"
#include "math.h"
#include "../numerical_library/tqli.c"
#include "../numerical_library/tred2.c"
#include <stdio.h>
#include "../numerical_library/nrutil.h"
#include "../numerical_library/pythag.c"

#define N 7

void wypiszMacierz(float** macierz){
	int i,j;
	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			printf("%f ", macierz[i][j]);
		}
		printf("\n");
	}
}

void wypiszWektor(float* wektor){
	int i;
	for(i=1;i<=N;i++){
		printf("%f ", wektor[i]);
	}
}

float mnozenieWektorWektor(float* wektor1, float* wektor2){
	int i;
	float wynik=0.0;
	for(i=1;i<=N;i++){
		wynik+=wektor1[i]*wektor2[i];
	}
	return wynik;
}


float* mnozenieMacierzWektor(float** macierz, float* wektor){
	int i,j;
	float* wynik = vector(1,N);
	for(i=1;i<=N;i++){
		wynik[i]=0.0;
		for(j=1;j<=N;j++){
			wynik[i]+=wektor[j]*macierz[i][j];
		}
	}
	return wynik;
}

float** mnozenieMacierzy(float** macierzA, float** macierzB){
	int i,j,k;
	float** wynik = matrix(1,N,1,N);
	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			wynik[i][j]=0.0;
		}
	}
	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			for(k=1;k<=N;k++){
				wynik[i][j]+=macierzA[i][k]*macierzB[k][j];
			}
		}
	}
	return wynik;
}


float* kolumna(float** a, int n){
	int i;
	float *w= vector(1,N);
	for(i=1;i<=N;i++){
		w[i]=a[i][n];
	}
	return w;
}


int main(){
	float** A = matrix(1,N,1,N);
	int i,j;
	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			A[i][j]=sqrt(i+j);
		}
	}

	float** copyA = matrix(1,N,1,N);
	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			copyA[i][j]=A[i][j];
		}
	}

	float* d = vector(1,N);
	float* e = vector(1,N);
	float* beta = vector(1,N);

	tred2(copyA, N, d, e);

	float** T = matrix(1,N,1,N);
	float** Y = matrix(1,N,1,N);

	float** X;

	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			if(i==j) Y[i][j]=1.0;
			else Y[i][j]=0.0;
		}
	}

	tqli(d,e,N,Y);
	printf("Wektor d\n");
	wypiszWektor(d);

	X=mnozenieMacierzy(copyA,Y);
	
	float* xk = vector(1,N);
	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			xk[j]=X[j][i];
		}
		beta[i]=mnozenieWektorWektor(xk, mnozenieMacierzWektor(A,xk))/mnozenieWektorWektor(xk,xk);
	}

	printf("\n");
	///********************//////

	float** W = matrix(1,N,1,N);

	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			W[i][j]=sqrt(i+j);
		}
	}

	float* x = vector(1,N);
	int k;
	int it;
	float l;
 	float m;
 	int o;
	float lambda=0.0;
	int z;
	int w;

	float* xn = vector(1,N);

	float norma;

	FILE* fp = fopen("dane.dat", "w");

	for(k=1;k<=N;k++){
		xn[k]=0.0;
	}

	for(k=1;k<=7;k++){
		for(w=1;w<=N;w++)
			x[w]=1.0;

		for(it=1;it<=8;it++){

			xn=mnozenieMacierzWektor(W,x);
			l=mnozenieWektorWektor(x,xn);
			m=mnozenieWektorWektor(x,x);
			lambda=l/m;
			norma= sqrt(mnozenieWektorWektor(xn,xn));
			for(z=1;z<=N;z++){	
				xn[z]/=norma;
				x[z]=xn[z];
			}
			printf("%0.4f\n", k, it, lambda);
			// fprintf(fp, "%0.4f\n", k, it, lambda);
		}
			fprintf(fp, "%0.4f\n", k, it, lambda);
	for(i=1;i<=N;i++){
		for(j=1;j<=N;j++){
			W[i][j]=W[i][j]-lambda*x[i]*x[j];
		}
	}
}
	
	return 0;
}
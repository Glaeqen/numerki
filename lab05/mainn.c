#include <stdio.h>
#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/tred2.c"
#include "../numerical_library/tqli.c"
#include "../numerical_library/pythag.c"

void saveMatrixToFile(const char *fileName, float **matrixToSave, int size);
void matrixTimesMatrix(float **output, float **first, float **second, int size);
void matrixMinusMatrix(float **leftMatrix, float **rightMatrix, float **outputMatrix, int size);
void fillVectorWith1(float *vector, int size);
void matrixTimesVector(float **leftMatrix, float *rightVector, float *outputVector, int size);
float vectorTimesVector(float *leftVector, float *rightVector, int size);
void vectorTimesVectorTensor(float *leftVector, float *rightVector, float **outputMatrix, int size);
float getNormOfVector(float *vector, int size);
void scalarTimesVector(float *vector, float scalar, int size);
void scalarTimesMatrix(float **matrix, float scalar, int size);
void copyVector(float *sourceVector, float *targetVector, int size);
void copyMatrix(float **sourceMatrix, float **targetMatrix, int size);
void printMatrix(float **matrix, int size);
void printVector(float *vector, int size);

int main(void){
	// #1
	int n = 7;
	float **A = matrix(1, n, 1, n);
	float *d = vector(1, n);
	float *e = vector(1, n);

	for(int i=1; i<=n; i++){
		for(int j=1 ; j<=n; j++){
			A[i][j] = sqrt(i+j);
		}
	}


	// #2
	float **P = matrix(1, n, 1, n);
	copyMatrix(A, P, n);
	tred2(P, n, d, e);


	// #3
	saveMatrixToFile("pMatrix.dat", P, n);

	// #4
	float **T = matrix(1, n, 1, n);

	for(int i=1; i<=n; i++){
		for(int j=1; j<=n; j++){
			if(i == j) T[i][j] = 1;
			else T[i][j] = 0;
		}
	}

	tqli(d, e, n, T);

	FILE *f = fopen("lambdas.dat", "w");

	for(int i=1; i<=n; i++){
		fprintf(f, "%0.4f\n", d[i]);
	}

	fclose(f);

	// #5
	saveMatrixToFile("tMatrix.dat", T, n);


	float *x = vector(1, n);
	float *xTemp = vector(1, n);
	float **tempMatrix = matrix(1, n, 1, n);
	float **tempMatrix2 = matrix(1, n, 1, n);
	float l, m, lambda, norm;
	for(int k=1; k<=n; k++){
		fillVectorWith1(x, n);

			// printVector(x, n);
		for(int i=1; i<=8; i++){
			matrixTimesVector(A, x, xTemp, n);
			l = vectorTimesVector(x, xTemp, n);
			m = vectorTimesVector(x, x, n);
			lambda = l/m;
			norm = getNormOfVector(xTemp, n);
			scalarTimesVector(xTemp, (1/norm), n);
			copyVector(xTemp, x, n);
			printf("%d \t %d \t %0.3f \n", k, i, lambda);
		}
		vectorTimesVectorTensor(x, x, tempMatrix, n);
		scalarTimesMatrix(tempMatrix, lambda, n);
		matrixMinusMatrix(A, tempMatrix, tempMatrix2, n);
		copyMatrix(tempMatrix2, A, n);
	}






	// // #6
	// float **eigVecsA = matrix(1, n, 1, n);
	// matrixTimesMatrix(eigVecsA, P, T, n);
	// saveMatrixToFile("eigVecsA.dat", eigVecsA, n);

	// // #7
	// float **AtimesX = matrix(1, n, 1, n);
	// matrixTimesMatrix(AtimesX, A, eigVecsA, n);

	// float *vecBeta = vector(1, n);

	// for(int i=1; i<=n; i++){
	// 	vecBeta[i] = 0;
	// 	for(int j=1; j<=n; j++){
	// 		vecBeta[i] += eigVecsA[j][i] * AtimesX[j][i];
	// 	}
	// }

	// // #8
	// FILE *f = fopen("vecBeta.dat", "w");

	// for(int i=1; i<=n; i++){
	// 	fprintf(f, "%0.4f\n", vecBeta[i]);
	// }

	// fclose(f);

	// f = fopen("lambdas.dat", "w");

	// for(int i=1; i<=n; i++){
	// 	fprintf(f, "%0.4f\n", d[i]);
	// }

	// fclose(f);

	// free_matrix(A, 1, n, 1, n);
	// free_matrix(P, 1, n, 1, n);
	// free_matrix(T, 1, n, 1, n);
	// free_matrix(eigVecsA, 1, n, 1, n);
	// free_matrix(AtimesX, 1, n, 1, n);

	// free_vector(d, 1, n);
	// free_vector(e, 1, n);
	// free_vector(vecBeta, 1, n);
}


void saveMatrixToFile(const char *fileName, float **matrixToSave, int size){
	FILE *f = fopen(fileName, "w");

	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			fprintf(f, "%0.4f\t", matrixToSave[i][j]);
		}
		fprintf(f, "\n");
	}

	fclose(f);
}

void matrixTimesMatrix(float **output, float **first, float **second, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			output[i][j] = 0;
			for(int k=1; k<=size; k++){
				output[i][j] += first[i][k] * second[k][j];
			}
		}
	}
}

void matrixMinusMatrix(float **leftMatrix, float **rightMatrix, float **outputMatrix, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			outputMatrix[i][j] = leftMatrix[i][j] - rightMatrix[i][j];
		}
	}
}

void fillVectorWith1(float *vector, int size){
	for(int i=1; i<=size; i++){
		vector[i] = 1.0;
	}
}

void matrixTimesVector(float **leftMatrix, float *rightVector, float *outputVector, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			outputVector[i] += leftMatrix[i][j]*rightVector[j];
		}
	}
}

float vectorTimesVector(float *leftVector, float *rightVector, int size){
	float result = 0.0;
	for(int i=1; i<=size; i++){
		result += leftVector[i] * rightVector[i];
	}
	return result;
}

void vectorTimesVectorTensor(float *leftVector, float *rightVector, float **outputMatrix, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			outputMatrix[i][j] = leftVector[i]*rightVector[j];
		}
	}
}

float getNormOfVector(float *vector, int size){
	float underSqrt = 0;
	for(int i=1; i<=size; i++){
		underSqrt += vector[i]*vector[i];
	}
	return sqrt(underSqrt);
}

void scalarTimesVector(float *vector, float scalar, int size){
	for(int i=1; i<=size; i++){
		vector[i] *= scalar;
	}
}

void scalarTimesMatrix(float **matrix, float scalar, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			matrix[i][j] *= scalar;
		}
	}
}

void copyVector(float *sourceVector, float *targetVector, int size){
	for(int i=1; i<=size; i++){
		targetVector[i] = sourceVector[i];
	}
}

void copyMatrix(float **sourceMatrix, float **targetMatrix, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			targetMatrix[i][j] = sourceMatrix[i][j];
		}
	}
}

void printMatrix(float **matrix, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			printf("%0.3f ", matrix[i][j]);
		}
		printf("\n");
	}
}
void printVector(float *vector, int size){
	for(int i=1; i<=size; i++){
		printf("%0.3f\n", vector[i]);
	}
}

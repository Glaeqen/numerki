#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gaussj.c"

#include <stdio.h>
#include <math.h>

float foo(float x){
	return log(x*x*x + 3*x*x + x + 0.1)*sin(18*x);
}

float simpson(float a, float b, int n){
	return (b-a)/(1 << (n+1));
}

float milne(float a, float b, int n){
	return (b-a)/(1 << (n+2));
}

float fsimpson(float a, float b, int n){
	float result = 0;
	float h = simpson(a, b, n);
	for(int i=0; i<=(1 << (n+1))/2-1; i++){
		result += (h/3)*(foo(a+h*2*i) + 4*foo(a+h*(2*i+1)) + foo(a+h*(2*i+2)));
	}
	return result;
}

float fmilne(float a, float b, int n){
	float result = 0;
	float h = milne(a, b, n);
	for(int i=0; i<=(1 << (n+2))/4-1; i++){
		result += (4*h/90)*(7*foo(a+h*(4*i))+32*foo(a+h*(4*i+1))+12*foo(a+h*(4*i+2))+32*foo(a+h*(4*i+3))+7*foo(a+h*(4*i+4)));
	}
	return result;
}

int main(){
	float a = 0;
	float b = 1;
	float **Dsimpson = matrix(0, 8, 0, 8);
	float **Dmilne = matrix(0, 8, 0, 8);

	for(int n=0; n<=8; n++){
		Dsimpson[n][0] = fsimpson(a, b, n);
		Dmilne[n][0] = fmilne(a, b, n);
	}

	for(int k=1; k<=8; k++){
		for(int m = k; m<=8; m++){
			Dsimpson[m][k] = (pow(4,k)/(pow(4,k)-1))*Dsimpson[m][k-1] - (1/(pow(4,k)-1))*Dsimpson[m-1][k-1];
			Dmilne[m][k] = (pow(4,k)/(pow(4,k)-1))*Dmilne[m][k-1] - (1/(pow(4,k)-1))*Dmilne[m-1][k-1];
		}
	}

	FILE *f1 = fopen("simpson.dat", "w");
	FILE *f2 = fopen("milne.dat", "w");

	for(int i=0; i<=8; i++){
		for(int j=0; j<=i; j++){
			fprintf(f1, "%f\t", Dsimpson[i][j]);
			fprintf(f2, "%f\t", Dmilne[i][j]);
		}
		fprintf(f1, "\n");
		fprintf(f2, "\n");
	}

	fclose(f1);
	fclose(f2);

	return 0;
}
#include <stdio.h>
#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/tred2.c"
#include "../numerical_library/tqli.c"
#include "../numerical_library/pythag.c"

void copyMatrix(float **target, float **source, int size);
void saveMatrixToFile(const char *fileName, float **matrixToSave, int size);
void matrixTimesMatrix(float **output, float **first, float **second, int size);

int main(void){
	// #1
	int n = 5;
	float **A = matrix(1, n, 1, n);
	float *d = vector(1, n);
	float *e = vector(1, n);

	for(int i=1; i<=n; i++){
		for(int j=1; j<=n; j++){
			A[i][j] = sqrt(i+j);
		}
	}

	// #2
	float **P = matrix(1, n, 1, n);
	copyMatrix(P, A, n);
	tred2(P, n, d, e);

	// #3
	saveMatrixToFile("pMatrix.dat", P, n);

	// #4
	float **T = matrix(1, n, 1, n);

	for(int i=1; i<=n; i++){
		for(int j=1; j<=n; j++){
			if(i == j) T[i][j] = 1;
			else T[i][j] = 0;
		}
	}

	tqli(d, e, n, T);

	// #5
	saveMatrixToFile("tMatrix.dat", T, n);

	// #6
	float **eigVecsA = matrix(1, n, 1, n);
	matrixTimesMatrix(eigVecsA, P, T, n);
	saveMatrixToFile("eigVecsA.dat", eigVecsA, n);

	// #7
	float **AtimesX = matrix(1, n, 1, n);
	matrixTimesMatrix(AtimesX, A, eigVecsA, n);

	float *vecBeta = vector(1, n);

	for(int i=1; i<=n; i++){
		vecBeta[i] = 0;
		for(int j=1; j<=n; j++){
			vecBeta[i] += eigVecsA[j][i] * AtimesX[j][i];
		}
	}

	// #8
	FILE *f = fopen("vecBeta.dat", "w");

	for(int i=1; i<=n; i++){
		fprintf(f, "%0.4f\n", vecBeta[i]);
	}

	fclose(f);

	f = fopen("lambdas.dat", "w");

	for(int i=1; i<=n; i++){
		fprintf(f, "%0.4f\n", d[i]);
	}

	fclose(f);

	free_matrix(A, 1, n, 1, n);
	free_matrix(P, 1, n, 1, n);
	free_matrix(T, 1, n, 1, n);
	free_matrix(eigVecsA, 1, n, 1, n);
	free_matrix(AtimesX, 1, n, 1, n);

	free_vector(d, 1, n);
	free_vector(e, 1, n);
	free_vector(vecBeta, 1, n);
}

void copyMatrix(float **target, float **source, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			target[i][j] = source[i][j];
		}
	}
}

void saveMatrixToFile(const char *fileName, float **matrixToSave, int size){
	FILE *f = fopen(fileName, "w");

	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			fprintf(f, "%0.4f\t", matrixToSave[i][j]);
		}
		fprintf(f, "\n");
	}

	fclose(f);
}

void matrixTimesMatrix(float **output, float **first, float **second, int size){
	for(int i=1; i<=size; i++){
		for(int j=1; j<=size; j++){
			output[i][j] = 0;
			for(int k=1; k<=size; k++){
				output[i][j] += first[i][k] * second[k][j];
			}
		}
	}
}
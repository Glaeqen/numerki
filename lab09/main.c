#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gaussj.c"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const char *filename1 = "3.51G.dat";
const char *filename2 = "3.51g.dat";
const char *filename3 = "bvsa3.51.dat";
int N = 51;
float x0 = 2;
float sigma = 4;
int m = 4;

float g2(float x){
	float a0 = -x0*x0/2/(sigma*sigma);
	float a1 = x0/(sigma*sigma);
	float a2 = -1/2.0/(sigma*sigma);
	return exp(a0 + a1 * x + a2 * (x * x));
}

float g(float x){
	float randomNumber = rand()/(RAND_MAX+1.0);
	return g2(x)*(1+0.1*randomNumber-0.5);
}

float GfinalFunction(float x, float **b){
	return exp(b[1][1] + b[2][1]*x + b[3][1]*x*x + b[4][1]*x*x*x);
}



int main(void){
	srand(time(NULL));

	float x_min = -3*sigma + x0;
	float x_max = 3*sigma + x0;
	float deltaX = (x_max - x_min)/(N-1);


	float *FXvector = vector(1, N);
	float *FYvector = vector(1, N);

	for(int i=1; i<=N; i++){
		FXvector[i] = x_min + deltaX*(i-1);
		FYvector[i] = log(g(FXvector[i]));
	}

	float **Gmatrix = matrix(1, m, 1, m);

	for(int i=1; i<=m; i++){
		for(int k=1; k<=m; k++){
			Gmatrix[i][k] = 0.0;
			for(int j=1; j<=N; j++){
				Gmatrix[i][k] += pow(FXvector[j], i+k-2);
			}
		}
	}

	float **Rvector = matrix(1, m, 1, 1);

	for(int k=1; k<=m; k++){
		Rvector[k][1] = 0.0;
		for(int j=1; j<=N; j++){
			Rvector[k][1] += FYvector[j]*pow(FXvector[j], k-1);
		}
	}

	gaussj(Gmatrix, m, Rvector, 1);

	// Now Rvector is the b vector

	FILE *f = fopen(filename1, "w");
	FILE *g = fopen(filename2, "w");
	FILE *h = fopen(filename3, "w");

	int density = 1000;
	float newDeltaX = (x_max - x_min)/(density-1);

	for(int i=1; i<=density; i++){
		fprintf(f, "%0.4f\t%0.4f\n", x_min+newDeltaX*i, GfinalFunction(x_min+newDeltaX*i, Rvector));
	}

	for(int i=1; i<=N; i++){
		fprintf(g, "%0.4f\t%0.4f\n", FXvector[i], exp(FYvector[i]));
	}

	float a0 = -x0*x0/2/(sigma*sigma);
	float a1 = x0/(sigma*sigma);
	float a2 = -1/2.0/(sigma*sigma);

	fprintf(h, "1\ta0: %0.4f\tb1: %0.4f\n", a0, Rvector[1][1]);
	fprintf(h, "x\ta1: %0.4f\tb2: %0.4f\n", a1, Rvector[2][1]);
	fprintf(h, "x^2\ta2: %0.4f\tb3: %0.4f\n", a2, Rvector[3][1]);
	fprintf(h, "x^3\ta3: %0.4f\tb4: %0.8f\n", 0.0, Rvector[4][1]);

	fclose(f);
	fclose(g);
	fclose(h);


	return 0;
}
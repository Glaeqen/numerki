#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gaussj.c"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double f(double param){
	return pow(log(param) - param, 6) - 1;
}
double fpoch(double param){
    return 6*pow((log(param)-param),5)*((1/param)-1);
}

double f_2(double param){
    return pow(param,3)+2*pow(param,2)-3*param+4;
}
double f_2poch(double param){
    return 3*pow(param,2)+4*param-3;
}


int main(void){
	double x_0 = 3.0;
	double x__1 = 3.01;
	double xd = 1.0;
	double e_1, e_0, e__1;
	e_0 = fabs(x_0-xd);
	e__1 = fabs(x__1-xd);
	double p;

	double x_1;
	FILE *fp;
	fp = fopen("sieczne_1.dat", "w");
	// Sieczne f1
	for(int i=1; i<=20; i++){
		x_1 = x_0 - (f(x_0)*(x_0-x__1))/(f(x_0)-f(x__1));
		x__1 = x_0;
		x_0 = x_1;
		e_1 = fabs(x_1-xd);
		p = (log(e_0/e_1))/(log(e__1/e_0));
		e__1 = e_0;
		e_0 = e_1;
		fprintf(fp, "%d\t%25.15f\t%25.15f\t%25.15f\t%25.15f\n", i, f(x_1), x_1, e_1, p);
	}
	fclose(fp);
	// Newton f1
	fp = fopen("newton_1.dat", "w");
	x_0 = 3.0;
	e_0 = fabs(x_0-xd);
	e__1 = fabs(x__1-xd);
	for(int i=1; i<=20; i++){
		x_1 = x_0 - (f(x_0)/fpoch(x_0));
		e_1 = fabs(x_1-xd);
		p = (log(e_0/e_1))/(log(e__1/e_0));
		fprintf(fp, "%d\t%25.15f\t%25.15f\t%25.15f\t%25.15f\n", i, f(x_1), x_1, e_1, p);
		x__1 = x_0;
		x_0 = x_1;
		e__1 = e_0;
		e_0 = e_1;
	}
	fclose(fp);


	x_0 = -20.0;
	x__1 = -20.1;
	xd = -3.28427753730695;
	e_0 = fabs(x_0-xd);
	e__1 = fabs(x__1-xd);

	fp = fopen("sieczne_2.dat", "w");
	// Sieczne f2
	for(int i=1; i<=20; i++){
		x_1 = x_0 - (f_2(x_0)*(x_0-x__1))/(f_2(x_0)-f_2(x__1));
		x__1 = x_0;
		x_0 = x_1;
		e_1 = fabs(x_1-xd);
		p = (log(e_0/e_1))/(log(e__1/e_0));
		e__1 = e_0;
		e_0 = e_1;
		fprintf(fp, "%d\t%25.15f\t%25.15f\t%25.15f\t%25.15f\n", i, f_2(x_1), x_1, e_1, p);
	}
	fclose(fp);

	// Newton f2
	fp = fopen("newton_2.dat", "w");
	x_0 = -20.0;
	e_0 = fabs(x_0-xd);
	for(int i=1; i<=20; i++){
		x_1 = x_0 - (f_2(x_0)/f_2poch(x_0));
		e_1 = fabs(x_1-xd);
		p = (log(e_0/e_1))/(log(e__1/e_0));
		fprintf(fp, "%d\t%25.15f\t%25.15f\t%25.15f\t%25.15f\n", i, f_2(x_1), x_1, e_1, p);
		x__1 = x_0;
		x_0 = x_1;
		e__1 = e_0;
		e_0 = e_1;
	}
	fclose(fp);

}

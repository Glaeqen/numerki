#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gaussj.c"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

float f1(float x){
	return cos(2*x);
}


float calc(float x, float **M, float *X, float *Y, int n){
	float s = 0.0;
	for(int i=2; i<=n; i++){
		if(x >=X[i-1] && x <= X[i]){
			float hi = X[i]-X[i-1];
			float hi1 = X[i+1] - X[i];
			s = M[i-1][1]*pow(X[i]-x, 3)/(6*hi) + M[i][1]*pow(x-X[i-1], 3)/(6*hi)+((Y[i]-Y[i-1])/hi - hi*(M[i][1]-M[i-1][1])/6)*(x-X[i-1]) + Y[i-1] - M[i-1][1]*hi*hi/6;
			return s;
		}
	}
}

int main(void){
	int n = 5;
	const char *filename = "f2_5.dat";
	float *X = vector(1, n);
	float *Y = vector(1, n);

	float x_min = -5;
	float x_max = 5;

	float h = (x_max - x_min)/(n-1);

	for(int i=1; i<=n; i++){
		X[i] = x_min + h*(i-1); 
		Y[i] = f1(X[i]);
	}


	// A*M = D
	float **A = matrix(1, n, 1, n);
	float **M = matrix(1, n, 1, 1);
	float **D = matrix(1, n, 1, 1);


	for(int i=1; i<=n; i++){
		for(int j=1; j<=n; j++){
			A[i][j] = 0;
			if(i==j && i!=1 && i!=n){
				A[i][j] = 2;
			}
			if(i!=1 && i!= n){ 
				float hi = X[i]-X[i-1];
				float hi1 = X[i+1] - X[i];
				float lambda = hi1/(hi+hi1);
				float mi = 1 - lambda;
				A[i][i+1] = lambda;
				A[i][i-1] = mi;
			}
		}
	}
	A[1][1] = A[n][n] = 1;
	for(int i=1; i<=n; i++){
		for(int j=1; j<=n; j++){
			printf("%0.2f ", A[i][j]);
		}
		putchar('\n');
	}
	D[1][1] = D[n][1] = 0;
	for(int i=2; i<n; i++){
		float hi = X[i]-X[i-1];
		float hi1 = X[i+1] - X[i];
		D[i][1] = 6*((Y[i+1]-Y[i])/hi1-(Y[i]-Y[i-1])/hi)/(hi+hi1);
	}
	for(int i=1; i<=n; i++){
		printf("%0.2f\n", D[i][1]);
	}

	printf("\n\n\n\n");

	gaussj(A, n, D, 1);

	for(int i=1; i<=n; i++){
		printf("%0.2f\n", D[i][1]);
	}

	int dens = 150;

	float delta = (x_max-x_min)/dens;

	FILE *f = fopen(filename, "w");

	for(int i=1; i<dens; i++){
		fprintf(f, "%0.4f\t%0.4f\t%0.4f\n", x_min+delta*i, f1(x_min+delta*i), calc(x_min+delta*i, D, X, Y, n));
	}


	return 0;
}
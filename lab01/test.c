#include "../numerical_library/nr.h"
#include "../numerical_library/nrutil.h"
#include "../numerical_library/nrutil.c"
#include "../numerical_library/gaussj.c"
#include "math.h"
#include <stdio.h>

int main(void){
	float omega = 1;
	float v0 = 0;
	float A = 1;
	float h = 0.1;


	int n = 200;
	float **a, **b;
	a = matrix(1, n, 1, n);
	b = matrix(1, n, 1, 1);

	//a
	for(int i=1; i<=n; i++){
		for(int j=1; j<=n; j++){
			a[i][j] = 0.0; //Zerowanie
			if(i==j) a[i][j] = 1; //Diagonala = 1
			if(i>=3) a[i][i-1] = omega*omega*h*h - 2.0;
			if(i>=3) a[i][i-2] = 1;
		}
	}
	a[2][1] = -1;

	for(int i=1; i<=n; i++) b[i][1] = 0;
	b[1][1] = A;
	b[2][1] = v0*h;

	gaussj(a, n, b, 1);

	FILE *fp;
	fp = fopen("dane.dat", "w");
	for(int i=1; i<=n; i++){
		float t = h*(i-1);
		float dok = A*cos(omega*t);
		fprintf(fp, "%f %f %f\n", t, b[i][1], dok);
	}
	fclose(fp);

	return 0;
}
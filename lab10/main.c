#include <math.h>
#include <stdio.h>

float h = 0.1;
float x0 = -0.75;
float y0 = 1.75;
int maxIter = 1000;

float epsylon = 0.001;
const char *filename = "eps2.dat";

float delta = 0.0001;

float f(float x, float y){
	return 5/2.0*(x*x-y)*(x*x-y)+(1-x)*(1-x);
}

float dfdx(float x, float y){
	return (f(x+delta, y)-f(x-delta, y))/(2*delta);
}

float dfdy(float x, float y){
	return (f(x, y+delta)-f(x, y-delta))/(2*delta);
}

int main(){
	float ri[2] = {x0, y0};
	float deltaR[2] = {h*dfdx(ri[0], ri[1]), h*dfdy(ri[0], ri[1])};
	float riPlus1[2] = {ri[0] - deltaR[0], ri[1] - deltaR[1]};

	FILE* file = fopen(filename, "w");

	fprintf(file, "%0.6f\t%0.6f\n", ri[0], ri[1]);
	fprintf(file, "%0.6f\t%0.6f\n", riPlus1[0], riPlus1[1]);

	for(int i=0; i<maxIter && sqrt((riPlus1[0]-ri[0])*(riPlus1[0]-ri[0]) + (riPlus1[1]-ri[1])*(riPlus1[1]-ri[1])) > epsylon; i++){
		ri[0] = riPlus1[0];
		ri[1] = riPlus1[1];

		deltaR[0] = h*dfdx(ri[0], ri[1]);
		deltaR[1] = h*dfdy(ri[0], ri[1]);

		riPlus1[0] = ri[0] - deltaR[0];
		riPlus1[1] = ri[1] - deltaR[1];

		fprintf(file, "%0.6f\t%0.6f\n", riPlus1[0], riPlus1[1]);
	}

	fclose(file);

	// Przygotowanie funkcji do rysowania

	file = fopen("fxy.dat", "w");

	for(float x=-2; x<=2; x+=0.02){
		for(float y=-2; y<=2; y+=0.02){
			fprintf(file, "%0.6f\t%0.6f\t%0.6f\n", x, y, f(x, y));
		}
		fprintf(file, "\n");
	}
	fclose(file);



	return 0;
}
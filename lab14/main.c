#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define CONS (1/(1.f - exp(-1)))
#define RANDOMNUM (rand()/(RAND_MAX*1.f))

float f(float x){
	return exp(-x);
}

float g(float x, float y){
	return sin(x+y)/log(2 + x + y);
}

float z(float x, float y){
	return g(x,y)*f(x)*f(y);
}

float F(float x){
	return CONS*(1-exp(-x));
}

float XQ(){
	return -log(1 - (RANDOMNUM/CONS));
}

int main(){
	srand(time(NULL));
	int N = 1e5;
	float sum = 0;
	float rand1, rand2;
	float sumOfSquares = 0;
	FILE *f = fopen("first.dat", "w");
	for(int n = 10; n<=N; n*=10){
		for(int i=1; i<=n; i++){
			rand1 = RANDOMNUM;
			rand2 = RANDOMNUM;
			float a = z(rand1, rand2);
			sum += a;
			sumOfSquares += a*a;
		}
		float integral = sum/n;
		float deltaSq = (sumOfSquares - sum*sum/n)/(n-1);
		float stdDev = sqrt(deltaSq)/sqrt(n);
		fprintf(f, "%d\t%f\t%f\n", n, integral, stdDev);
		sum = sumOfSquares = 0;
	}
	fclose(f);
	f = fopen("second.dat", "w");
	for(int n = 10; n<=N; n*=10){
		for(int i=1; i<=n; i++){
			rand1 = XQ();
			rand2 = XQ();
			float a = g(rand1, rand2)/(CONS*CONS);
			sum += a;
			sumOfSquares += a*a;
		}
		float integral = sum/n;
		float deltaSq = (sumOfSquares - sum*sum/n)/(n-1);
		float stdDev = sqrt(deltaSq)/sqrt(n);
		fprintf(f, "%d\t%f\t%f\n", n, integral, stdDev);
		sum = sumOfSquares = 0;
	}
	fclose(f);

	int n[10] = {0};
	for(int i=1; i<=N; i++){
		float num = XQ();
		int index = num*10;
		n[index]++;
	}
	f = fopen("pi.dat", "w");
	float P[10] = {0.0};
	float dx = 1/10.f;
	for(int i=0; i<10; i++){
		P[i] = F((i+1)*dx) - F(i*dx);
	}
	for(int i=0; i<10; i++){
		fprintf(f, "%d\t%f\t%f\n", i+1, n[i]/(N*1.f), P[i]);
	}
	fclose(f);
	float chi = 0.0;
	for(int i=0; i<10; i++){
		chi += pow(n[i] - N*P[i], 2)/(N*P[i]);
	}
	f = fopen("chi.dat", "w");
	fprintf(f, "%f\n", chi);
	fclose(f);

	return 0;
}